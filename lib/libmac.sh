#!/bin/bash

# Based on https://github.com/mlpack/mlpack-wheels

# Sudo needed for macOS
use_sudo="sudo"

basedir=$(python3 lib/openblas_support.py)
$use_sudo cp -r $basedir/lib/* /usr/local/lib
$use_sudo cp $basedir/include/* /usr/local/include

# basedir=$(python3 lib/hdf5_support.py)
# $use_sudo cp -r $basedir/lib/* /usr/local/lib
# $use_sudo cp $basedir/include/* /usr/local/include
# $use_sudo cp $basedir/bin/* /usr/local/bin

basedir=$(python3 lib/gfortran_support.py)
# delocate only searches this directory for gfortran
$use_sudo mkdir /usr/local/gfortran/
$use_sudo mkdir /usr/local/gfortran/lib
$use_sudo cp -r $basedir/lib/ /usr/local/gfortran/lib
$use_sudo cp -r $basedir/lib/ /usr/local/lib

export DYLD_FALLBACK_LIBRARY_PATH=/usr/local/lib/